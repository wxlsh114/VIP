#coding=utf-8
from appium import webdriver
import unittest,time,os
from time import sleep
#from HTMLTestRunner import HTMLTestRunner
#from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
from appium.webdriver.common.touch_action import TouchAction

def login(self):
    driver=self.driver
    sleep(1)
    #driver.find_element_by_accessibility_id('允许').click()
    t=driver.switch_to_alert()
    t.accept()
    sleep(1)
    #跳 过
    driver.find_element_by_accessibility_id('跳 过').click()
    sleep(1)
    mo=driver.find_element_by_class_name('XCUIElementTypeTextField')
    mo.click()
    #18923236756
    mo.set_value('15100000011')
    #driver.find_element_by_accessibility_id('完成').click()
    sleep(1)
    pwd=driver.find_element_by_class_name('XCUIElementTypeSecureTextField')
    pwd.click()
    pwd.set_value('123456')
    #driver.find_element_by_accessibility_id('完成').click()
    sleep(1)
    driver.find_elements_by_class_name('XCUIElementTypeButton')[1].click()
    sleep(3)
    t2=EC.alert_is_present()(driver)
    if t2:
        t2.accept()
        sleep(3)
    #someone already logged in
    another=driver.find_elements_by_accessibility_id('确定')
    if len(another)!=0:
        driver.find_element_by_accessibility_id('确定').click()
        sleep(3)
    #test now
    driver.find_element_by_accessibility_id('开始测试').click()
    sleep(3)
    driver.find_element_by_accessibility_id('点击开始录音').click()
    sleep(2)
    driver.find_element_by_accessibility_id('停止录音').click()
    sleep(3)
    driver.find_element_by_accessibility_id('有听到声音').click()
    sleep(2)
    driver.find_element_by_accessibility_id('下一步').click()
    sleep(2)
    driver.find_element_by_accessibility_id('下一步').click()
    sleep(2)
    driver.find_element_by_accessibility_id('完成测试').click()
    sleep(2)
        
def logout(self):
    driver=self.driver
    sleep(2)
    driver.find_element_by_accessibility_id('个人中心').click()
    sleep(2)
    driver.find_element_by_accessibility_id('退出登录').click()
    sleep(2)
    driver.find_element_by_accessibility_id('确定').click()
    sleep(2)

def turnpage_play(self):
    driver=self.driver
    sleep(1)
    #turn page left/right
    driver.swipe(600,600,-550,0,500)
    sleep(1)
    driver.swipe(50,600,550,0,500)
    sleep(1)
    #Add play code here
    lis=driver.find_elements_by_accessibility_id('play')
    if len(lis)!=0:
        driver.find_element_by_accessibility_id('play').click()
        sleep(8)
        driver.find_element_by_accessibility_id('play').click()
        sleep(2)
    sleep(1)
