#coding=utf-8
from appium import webdriver
import unittest,time,os
from time import sleep
from HTMLTestRunner import HTMLTestRunner
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.support import expected_conditions as EC
from pub_Teacher import login,logout,turnpage_play

class TestTeacher(unittest.TestCase):

    def setUp(self):
        # set up appium
        desired_caps = {}
        #desired_caps['appium-version'] = '1.7.1'
        desired_caps['platformName'] = 'iOS'
        desired_caps['browserName']=''
        desired_caps['automationName'] = 'XCUITest'
        desired_caps['platformVersion'] = '10.3'
        desired_caps['deviceName'] = 'iPad'
        desired_caps['app'] = os.path.abspath('../pnlPianoClassing_teacher_iPad.ipa')
        desired_caps['udid'] = '4328fd8e3a20ebc4d79dd6e65059b563555dbfe2'
        desired_caps['fullReset'] = True
        #desired_caps['clearSystemFiles'] = True
        desired_caps['xcodeOrgId'] = 'P2ZL3LJVPZ'
        #desired_caps['xcodeSigning'] = 'iPhone Developer'

        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
        #self.driver.implicitly_wait(30)
        sleep(3)
        
    def searchAddMusicClassroom(self):
        driver=self.driver
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n033:进入教室搜索添加乐谱----开始:'+now)
        login(self)
        sleep(3)
        lis1=driver.find_elements_by_accessibility_id('本日暂时没有课程安排')
        if len(lis1)!=0:
            for i in range(2,8):
                #bu=driver.find_elements_by_class_name('XCUIElementTypeButton')
                bu=driver.find_element_by_xpath('//XCUIElementTypeCell/XCUIElementTypeButton['+str(i)+']')
                bu.click()
                sleep(2)
                lis2=driver.find_elements_by_accessibility_id('历史课单')
                if len(lis2)!=0:
                    driver.find_element_by_xpath('//XCUIElementTypeButton[@name="进入教室"]').click()
                    sleep(5)
                    break
        else:
            driver.find_elements_by_xpath('//XCUIElementTypeButton[@name="进入教室"]')[1].click()
            sleep(5)
        sleep(2)
        t1=EC.alert_is_present()(driver)
        if t1:
            t1.accept()
            sleep(2)
        driver.find_element_by_accessibility_id('查看上课乐谱').click()
        sleep(2)
        t2=EC.alert_is_present()(driver)
        if t2:
            t2.accept()
            sleep(2)
        items=driver.find_elements_by_class_name('XCUIElementTypeCell')
        i=len(items)
        print(str(i))
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf3='./'+now+'_033b_beforeAddedMusic_R.png'
        driver.get_screenshot_as_file(sf3)
        sleep(2)
        driver.find_element_by_accessibility_id('上传乐谱').click()
        sleep(3)
        #hot search
        driver.find_element_by_accessibility_id('搜索书名或者曲目名').click()
        sleep(3)
        driver.find_element_by_accessibility_id('车尔尼').click()
        sleep(3)
        driver.find_element_by_accessibility_id('小提琴').click()
        sleep(3)
        driver.find_element_by_accessibility_id('手风琴').click()
        sleep(3)
        driver.find_element_by_accessibility_id('古筝').click()
        sleep(2)
        driver.find_element_by_accessibility_id('钢琴').click()
        sleep(3)
        #first music
        driver.find_element_by_accessibility_id('包含该曲目').click()
        sleep(3)
        driver.find_element_by_accessibility_id('车尔尼299 No.09').click()
        sleep(8)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf2='./'+now+'_033b_searchedMusicByHotDetail_R.png'
        driver.get_screenshot_as_file(sf2)
        sleep(2)
        turnpage_play(self)
        sleep(2)
        #TouchAction(self.driver).press(x=25,y=41).wait(100).release().perform()
        driver.find_element_by_accessibility_id('red back').click()
        sleep(3)
        driver.find_element_by_accessibility_id('添加').click()
        sleep(3)
        #TouchAction(self.driver).press(x=25,y=41).wait(100).release().perform()
        driver.find_element_by_accessibility_id('red back').click()
        sleep(3)
        driver.find_element_by_accessibility_id('取消').click()
        sleep(3)
        driver.find_element_by_accessibility_id('全部').click()
        sleep(2)
        #whole music name
        s=driver.find_element_by_class_name('XCUIElementTypeTextField')
        s.click()
        s.set_value('车尔尼299 No.02')
        sleep(2)
        driver.find_element_by_accessibility_id('Search').click()
        sleep(4)
        driver.find_element_by_accessibility_id('包含该曲目').click()
        sleep(3)
        driver.find_elements_by_accessibility_id('车尔尼299 No.02')[1].click()
        sleep(8)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf1='./'+now+'_033b_searchedMusicByWholenameDetail_R.png'
        driver.get_screenshot_as_file(sf1)
        sleep(2)
        turnpage_play(self)
        sleep(2)
        #TouchAction(self.driver).press(x=25,y=41).wait(100).release().perform()
        driver.find_element_by_accessibility_id('red back').click()
        sleep(3)
        t1=driver.find_elements_by_accessibility_id('添加')
        if len(t1)!=0:
            driver.find_element_by_accessibility_id('添加').click()
        sleep(3)
        #TouchAction(self.driver).press(x=25,y=41).wait(100).release().perform()
        driver.find_element_by_accessibility_id('red back').click()
        sleep(3)
        driver.find_element_by_accessibility_id('取消').click()
        sleep(2)
        driver.find_element_by_accessibility_id('全部').click()
        sleep(2)
        #keyword
        s=driver.find_element_by_class_name('XCUIElementTypeTextField')
        s.click()
        s.set_value('299 No.07')
        sleep(2)
        driver.find_element_by_accessibility_id('Search').click()
        sleep(4)
        driver.find_element_by_accessibility_id('包含该曲目').click()
        sleep(3)
        driver.find_element_by_accessibility_id('车尔尼299 No.07').click()
        sleep(8)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf0='./'+now+'_033b_searchedMusicByKeywordDetail_R.png'
        driver.get_screenshot_as_file(sf0)
        sleep(2)
        turnpage_play(self)
        sleep(2)
        #TouchAction(self.driver).press(x=25,y=41).wait(100).release().perform()
        driver.find_element_by_accessibility_id('red back').click()
        sleep(3)
        t2=driver.find_elements_by_accessibility_id('添加')
        if len(t2)!=0:
            driver.find_element_by_accessibility_id('添加').click()
        sleep(3)
        #TouchAction(self.driver).press(x=25,y=41).wait(100).release().perform()
        driver.find_element_by_accessibility_id('red back').click()
        sleep(3)
        driver.find_element_by_accessibility_id('取消').click()
        sleep(2)
        #TouchAction(self.driver).press(x=25,y=41).wait(100).release().perform()
        driver.find_element_by_accessibility_id('red back').click()
        sleep(3)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf='./'+now+'_033b_afterAddedMusic_R.png'
        driver.get_screenshot_as_file(sf)
        sleep(2)
        #TouchAction(self.driver).press(x=25,y=41).wait(100).release().perform()
        driver.find_element_by_accessibility_id('red back').click()
        sleep(3)
        driver.find_element_by_accessibility_id('退出课程').click()
        sleep(3)
        driver.find_element_by_accessibility_id('其他原因退出').click()
        sleep(3)
        logout(self)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n033:进入教室搜索添加乐谱----结束:'+now)

    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    testunit=unittest.TestSuite()
    testunit.addTest(TestTeacher('searchAddMusicClassroom'))
    now=time.strftime('%Y-%m-%d %H_%M_%S')
    filename='./'+now+'_033b_R.html'
    fp=open(filename,'wb')
    runner=HTMLTestRunner(stream=fp,title='测试老师版iOS10.3.3/iPad Mini3真机(进入教室搜索添加乐谱)测试报告by Appium',
                          description='自动化测试脚本运行状态:')
    runner.run(testunit)
    fp.close()
