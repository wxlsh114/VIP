#coding=utf-8
from appium import webdriver
import unittest,time,os
from time import sleep
from HTMLTestRunner import HTMLTestRunner
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
#from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.support import expected_conditions as EC
from pub_Teacher import login,logout

class TestTeacher(unittest.TestCase):

    def setUp(self):
        # set up appium
        desired_caps = {}
        #desired_caps['appium-version'] = '1.7.1'
        desired_caps['platformName'] = 'iOS'
        desired_caps['browserName']=''
        desired_caps['automationName'] = 'XCUITest'
        desired_caps['platformVersion'] = '11.0'
        desired_caps['deviceName'] = 'iPad'
        desired_caps['app'] = os.path.abspath('../pnlPianoClassing_teacher_iPad.ipa')
        desired_caps['udid'] = '06a3e0b8854c2626c83be6ce191ecd775510acfa'
        desired_caps['fullReset'] = True
        #desired_caps['clearSystemFiles'] = True
        desired_caps['xcodeOrgId'] = 'P2ZL3LJVPZ'
        #desired_caps['xcodeSigning'] = 'iPhone Developer'

        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
        #self.driver.implicitly_wait(30)
        sleep(3)

    def waitForStuMorethan1Min(self):
        driver=self.driver
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n009:等待学生进入（老师进入，学生超过1分钟未进入）----开始:'+now)
        login(self)
        sleep(3)
        lis1=driver.find_elements_by_accessibility_id('本日暂时没有课程安排')
        if len(lis1)!=0:
            for i in range(2,8):
                #bu=driver.find_elements_by_class_name('XCUIElementTypeButton')
                bu=driver.find_element_by_xpath('//XCUIElementTypeCell/XCUIElementTypeButton['+str(i)+']')
                bu.click()
                sleep(3)
                lis2=driver.find_elements_by_accessibility_id('历史课单')
                if len(lis2)!=0:
                    driver.find_element_by_xpath('//XCUIElementTypeButton[@name="进入教室"]').click()
                    sleep(5)
                    break
        else:
            driver.find_elements_by_xpath('//XCUIElementTypeButton[@name="进入教室"]')[1].click()
            sleep(5)
        sleep(2)
        t1=EC.alert_is_present()(driver)
        if t1:
            t1.accept()
            sleep(3)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf0='./'+now+'_009b_enteredClassroom_R.png'
        driver.save_screenshot(sf0)
        sleep(58)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf1='./'+now+'_009b_waitMorethan1Min_R.png'
        driver.save_screenshot(sf1)
        sleep(2)
        flag=driver.find_elements_by_accessibility_id('学生长时间未进入教室 正在请求客服帮助')
        print(str(len(flag)))
        sleep(2)
        driver.find_element_by_accessibility_id('退出课程').click()
        sleep(3)
        driver.find_element_by_accessibility_id('其他原因退出').click()
        sleep(3)
        logout(self)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n009:等待学生进入（老师进入，学生超过1分钟未进入）----结束:'+now)

    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    testunit=unittest.TestSuite()
    testunit.addTest(TestTeacher('waitForStuMorethan1Min'))
    now=time.strftime('%Y-%m-%d %H_%M_%S')
    filename='./'+now+'_009b_R.html'
    fp=open(filename,'wb')
    runner=HTMLTestRunner(stream=fp,title='测试老师版iOS11.0.3/iPad5真机(等待学生进入（老师进入，学生超过1分钟未进入）)测试报告by Appium',
                          description='自动化测试脚本运行状态:')
    runner.run(testunit)
    fp.close()
