#coding=utf-8
from appium import webdriver
import unittest,time,os
from time import sleep
from HTMLTestRunner import HTMLTestRunner
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
#from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.support import expected_conditions as EC
from pub_Teacher import login,logout

class TestTeacher(unittest.TestCase):

    def setUp(self):
        # set up appium
        desired_caps = {}
        #desired_caps['appium-version'] = '1.7.1'
        desired_caps['platformName'] = 'iOS'
        desired_caps['browserName']=''
        desired_caps['automationName'] = 'XCUITest'
        desired_caps['platformVersion'] = '11.0'
        desired_caps['deviceName'] = 'iPad'
        desired_caps['app'] = os.path.abspath('../pnlPianoClassing_teacher_iPad.ipa')
        desired_caps['udid'] = '06a3e0b8854c2626c83be6ce191ecd775510acfa'
        desired_caps['fullReset'] = True
        #desired_caps['clearSystemFiles'] = True
        desired_caps['xcodeOrgId'] = 'P2ZL3LJVPZ'
        #desired_caps['xcodeSigning'] = 'iPhone Developer'

        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
        #self.driver.implicitly_wait(30)
        sleep(3)

    def uploadMusic(self):
        driver=self.driver
        sleep(3)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n005:上传乐谱----开始:'+now)
        login(self)
        sleep(4)
        lis1=driver.find_elements_by_accessibility_id('本日暂时没有课程安排')
        if len(lis1)!=0:
            for i in range(2,8):
                #bu=driver.find_elements_by_class_name('XCUIElementTypeButton')
                bu=driver.find_element_by_xpath('//XCUIElementTypeCell/XCUIElementTypeButton['+str(i)+']')
                bu.click()
                sleep(3)
                lis2=driver.find_elements_by_accessibility_id('查看乐谱')
                if len(lis2)!=0:
                    driver.find_element_by_xpath('//XCUIElementTypeCell[2]/XCUIElementTypeButton[2]').click()
                    sleep(2)
                    break
        else:
            #查看乐谱 top
            driver.find_element_by_xpath('//XCUIElementTypeOther/XCUIElementTypeButton[3]').click()
            sleep(2)
        t1=EC.alert_is_present()(driver)
        if t1:
            t1.accept()
            sleep(3)
        driver.find_element_by_accessibility_id('上传乐谱').click()
        sleep(3)
        driver.find_element_by_accessibility_id('钢琴').click()
        sleep(3)
        driver.find_element_by_accessibility_id('搜索书名或者曲目名').click()
        sleep(3)
        driver.find_element_by_accessibility_id('车尔尼').click()
        sleep(3)
        driver.find_element_by_accessibility_id('包含该曲目').click()
        sleep(3)
        driver.find_element_by_accessibility_id('添加').click()
        sleep(3)
        driver.find_element_by_accessibility_id('red back').click()
        sleep(2)
        driver.find_element_by_accessibility_id('red back').click()
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf1='./'+now+'_005b_afterUploadedMusic_R.png'
        driver.get_screenshot_as_file(sf1)
        sleep(2)
        #first music
        driver.find_element_by_class_name('XCUIElementTypeCell').click()
        sleep(3)
        driver.swipe(600,600,-550,0,500)
        sleep(2)
        driver.swipe(20,600,480,0,500)
        sleep(2)
        #Add play code here
        lis=driver.find_elements_by_accessibility_id('play')
        if len(lis)!=0:
            driver.find_element_by_accessibility_id('play').click()
            sleep(8)
            driver.find_element_by_accessibility_id('play').click()
            sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf2='./'+now+'_005b_uploadedMusicDetail_R.png'
        driver.get_screenshot_as_file(sf2)
        sleep(2)
        driver.find_element_by_accessibility_id('red back').click()
        sleep(2)
        driver.find_element_by_accessibility_id('red back').click()
        sleep(3)
        logout(self)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n005:上传乐谱----结束:'+now)

    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    testunit=unittest.TestSuite()
    testunit.addTest(TestTeacher('uploadMusic'))
    now=time.strftime('%Y-%m-%d %H_%M_%S')
    filename='./'+now+'_005b_R.html'
    fp=open(filename,'wb')
    runner=HTMLTestRunner(stream=fp,title='测试老师版iOS11.0.3/iPad5真机(上传乐谱)测试报告by Appium',
                          description='自动化测试脚本运行状态:')
    runner.run(testunit)
    fp.close()
