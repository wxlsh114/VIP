#coding=utf-8
import unittest,time,os
from time import sleep
from appium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from HTMLTestRunner import HTMLTestRunner
#from appium.webdriver.common.touch_action import TouchAction
#from pub_Teacher import login,logout,turnpage_play
from readfemale103e import getInfo


# Returns abs path relative to this file and not cwd
PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)


class eBaotong(unittest.TestCase):

    def setUp(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '6.0'
        desired_caps['automationName'] = 'UIAutomator2'
        desired_caps['deviceName'] = 'kenzo'
        #desired_caps['udid'] = 'HMKNW17225011700'
        desired_caps['app'] = PATH('ebao.apk')
        desired_caps['appPackage'] = 'com.bocommlife.pta'
        desired_caps['unicodeKeyboard'] = True
        desired_caps['resetKeyboard'] = True
        desired_caps['noReset'] = True

        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
        sleep(3)

    def tearDown(self):
        # end the session
        self.driver.quit()

    def modify_female(self):
        driver=self.driver
        f=getInfo(self)
        sleep(3)
        #我的
        driver.find_element_by_xpath('//*[@resource-id="android:id/content"]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]\
        /android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup[2]\
        /android.view.ViewGroup[5]').click()
        sleep(2)
        driver.find_element_by_xpath('//*[@text="客户管理"]').click()
        sleep(2)
        driver.find_element_by_xpath('//*[@resource-id="android:id/content"]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]\
        /android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[3]\
        /android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup[2]').click()
        #driver.swipe(50,700,50,500,1000)
        sleep(1)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf0='./'+now+'_103e_orginalfemale_R.png'
        driver.get_screenshot_as_file(sf0)
        sleep(2)
        name=driver.find_element_by_xpath('//android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]\
        /android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]\
        /android.view.ViewGroup[1]/android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]\
        /android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.widget.EditText[1]')
        name.click()
        name.clear()
        #name.set_value('葛雨釉')
        #name.send_keys('葛宇优')
        name.send_keys(f[0])
        sleep(1)
        zjh=driver.find_element_by_xpath('//android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]\
        /android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]\
        /android.view.ViewGroup[1]/android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]\
        /android.view.ViewGroup[2]/android.view.ViewGroup[4]/android.widget.EditText[1]')
        zjh.click()
        zjh.clear()
        #zjh.set_value('GY345678971')
        zjh.set_value(f[1])
        sleep(1)
        driver.swipe(50,800,50,400,1000)
        sleep(1)
        sjh=driver.find_element_by_xpath('//android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]\
        /android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]\
        /android.view.ViewGroup[1]/android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]\
        /android.view.ViewGroup[2]/android.view.ViewGroup[8]/android.widget.EditText[1]')
        sjh.click()
        sjh.clear()
        #sjh.send_keys('13811118971')
        sjh.send_keys(f[2])
        sleep(1)
        driver.press_keycode('66')
        sleep(1)
        email=driver.find_element_by_xpath('//android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]\
        /android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]\
        /android.view.ViewGroup[1]/android.widget.ScrollView[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]\
        /android.view.ViewGroup[2]/android.view.ViewGroup[10]/android.widget.EditText[1]')
        email.click()
        email.clear()
        #email.send_keys('13811118971@126.com')
        email.send_keys(str(f[2])+'@126.com')
        sleep(1)
        driver.press_keycode('66')
        sleep(1)
        driver.find_element_by_xpath('//*[@resource-id="android:id/content"]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]\
        /android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[1]/android.view.ViewGroup[2]').click()
        sleep(5)
        #driver.find_element_by_xpath('//*[@text="返回"]').click()
        driver.find_element_by_android_uiautomator('new UiSelector().text("返回")').click()
        sleep(3)

if __name__ == '__main__':
    testunit=unittest.TestSuite()
    testunit.addTest(eBaotong('modify_female'))
    now=time.strftime('%Y-%m-%d %H_%M_%S')
    filename='./'+now+'_103e_result_R.html'
    fp=open(filename,'wb')
    runner=HTMLTestRunner(stream=fp,title='交银e保通测试版android6.0.1真机(RedMi Note3)[客户女资料变更]测试报告by Appium',
                          description='自动化测试脚本运行状态:')
    runner.run(testunit)
    fp.close()
