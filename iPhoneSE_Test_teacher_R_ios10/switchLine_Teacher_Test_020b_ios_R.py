#coding=utf-8
from appium import webdriver
import unittest,time,os
from time import sleep
from HTMLTestRunner import HTMLTestRunner
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
#from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.support import expected_conditions as EC
from pub_Teacher import login,logout

class TestTeacher(unittest.TestCase):

    def setUp(self):
        # set up appium
        desired_caps = {}
        #desired_caps['appium-version'] = '1.7.1'
        desired_caps['platformName'] = 'iOS'
        desired_caps['browserName']=''
        desired_caps['automationName'] = 'XCUITest'
        desired_caps['platformVersion'] = '10.3'
        desired_caps['deviceName'] = 'iPhoneSE'
        desired_caps['app'] = os.path.abspath('../Text-Vip-Gangqin-Teacher.ipa')
        desired_caps['udid'] = 'efa32991905b2cb5d9fdf2bbf66ba5920cbf79e7'
        desired_caps['fullReset'] = True
        desired_caps['clearSystemFiles'] = True
        desired_caps['xcodeOrgId'] = 'P2ZL3LJVPZ'
        #desired_caps['xcodeSigning'] = 'iPhone Developer'

        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
        #self.driver.implicitly_wait(30)
        sleep(3)

    def switchLine(self):
        driver=self.driver
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n020:切换线路----开始:'+now)
        login(self)
        sleep(4)
        #本日暂时没有课程安排;can't see in iPhoneSE
        lis1=driver.find_elements_by_accessibility_id('pic_1')
        if len(lis1)!=0:
            for i in range(2,8):
                #bu=driver.find_elements_by_class_name('XCUIElementTypeButton')
                bu=driver.find_element_by_xpath('//XCUIElementTypeOther[1]/XCUIElementTypeButton['+str(i)+']')
                bu.click()
                sleep(3)
                lis2=driver.find_elements_by_accessibility_id('历史课单')
                if len(lis2)!=0:
                    #历史课单 middle
                    driver.find_element_by_xpath('//XCUIElementTypeButton[@name="进入教室"]').click()
                    sleep(5)
                    break
        else:
            #历史课单 top
            driver.swipe(650,500,0,-470,1000)
            sleep(2)
            driver.find_elements_by_accessibility_id('进入教室')[1].click()
            sleep(5)
        t=EC.alert_is_present()(driver)
        if t:
            t.accept()
            sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf0='./'+now+'_020b_beforeSwitch_R.png'
        driver.save_screenshot(sf0)
        sleep(3)
        driver.find_element_by_accessibility_id('更多').click()
        sleep(3)
        driver.find_element_by_accessibility_id('切换线路').click()
        sleep(2)
        driver.find_element_by_accessibility_id('确定').click()
        sleep(10)
        driver.find_element_by_accessibility_id('更多').click()
        sleep(3)
        driver.find_element_by_accessibility_id('切换线路').click()
        sleep(2)
        driver.find_element_by_accessibility_id('确定').click()
        sleep(10)
        driver.find_element_by_accessibility_id('退出').click()
        sleep(2)
        driver.find_element_by_accessibility_id('其他原因退出').click()
        sleep(3)
        logout(self)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n020:切换线路----结束:'+now)

    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    testunit=unittest.TestSuite()
    testunit.addTest(TestTeacher('switchLine'))
    now=time.strftime('%Y-%m-%d %H_%M_%S')
    filename='./'+now+'_020b_R.html'
    fp=open(filename,'wb')
    runner=HTMLTestRunner(stream=fp,title='测试老师版iOS10.3.3/iPhoneSE真机(切换线路)测试报告by Appium',
                          description='自动化测试脚本运行状态:')
    runner.run(testunit)
    fp.close()
