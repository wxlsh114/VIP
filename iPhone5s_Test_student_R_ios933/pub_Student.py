#coding=utf-8
from appium import webdriver
import unittest,time,os
from time import sleep
#from HTMLTestRunner import HTMLTestRunner
#from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support import expected_conditions as EC
#from appium.webdriver.common.touch_action import TouchAction


def login(self):
    driver=self.driver
    sleep(2)
    #driver.find_element_by_accessibility_id('好').click()
    t=EC.alert_is_present()(driver)
    if t:
        t.accept()
        sleep(3)
    driver.find_element_by_accessibility_id('登录').click()
    sleep(1)
    mo=driver.find_element_by_class_name('XCUIElementTypeTextField')
    mo.click()
    mo.set_value('14100000012')
    sleep(1)
    pwd=driver.find_element_by_class_name('XCUIElementTypeSecureTextField')
    pwd.click()
    pwd.set_value('123456')
    sleep(1)
    #登录
    #driver.find_element_by_xpath('//XCUIElementTypeButton[@name="登录"]').click()
    driver.find_elements_by_class_name('XCUIElementTypeButton')[3].click()
    sleep(3)
    #someone already logged in
    another=driver.find_elements_by_accessibility_id('确定')
    if len(another)!=0:
        driver.find_element_by_accessibility_id('确定').click()
        sleep(3)
    t2=EC.alert_is_present()(driver)
    if t2:
        t2.accept()
        sleep(3)
    #test now
    driver.find_element_by_accessibility_id('开始测试').click()
    sleep(2)
    driver.find_element_by_accessibility_id('点击开始录音').click()
    sleep(3)
    driver.find_element_by_accessibility_id('停止录音').click()
    sleep(2)
    driver.find_element_by_accessibility_id('有听到声音').click()
    sleep(2)
    driver.find_element_by_accessibility_id('下一步').click()
    sleep(2)
    driver.find_element_by_accessibility_id('下一步').click()
    sleep(2)
    driver.find_element_by_accessibility_id('完成测试').click()
    sleep(2)
    #Got it.
    driver.find_element_by_accessibility_id('ic guide btn').click()
    sleep(2)
        
def logout(self):
    driver=self.driver
    driver.find_element_by_accessibility_id('个人中心').click()
    sleep(3)
    driver.swipe(5,500,0,-220,500)
    sleep(2)
    driver.find_element_by_accessibility_id('退出登录').click()
    sleep(2)
    driver.find_element_by_accessibility_id('确定').click()
    sleep


def turnpage_play(self):
    driver=self.driver
    sleep(2)
    #turn page left/right
    page=driver.find_elements_by_accessibility_id('ic next1')
    if (len(page)!=0 and driver.find_element_by_accessibility_id('ic next1').is_displayed()):
        driver.find_element_by_xpath('//XCUIElementTypeButton[@name="ic next1"]').click()
        sleep(2)
        driver.find_element_by_xpath('//XCUIElementTypeButton[@name="ic next2"]').click()
        sleep(2)
    sleep(1)
    #Add play code here
    pb=driver.find_elements_by_accessibility_id('play')
    if len(pb)!=0:
        driver.find_element_by_accessibility_id('play').click()
        sleep(8)
        driver.find_element_by_accessibility_id('play').click()
        sleep(2)
    sleep(1)
