#coding=utf-8
from appium import webdriver
import unittest,time,os
from time import sleep
from HTMLTestRunner import HTMLTestRunner
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.support import expected_conditions as EC
from pub_Teacher import login,logout

class TestTeacher(unittest.TestCase):

    def setUp(self):
        # set up appium
        desired_caps = {}
        #desired_caps['appium-version'] = '1.7.1'
        desired_caps['platformName'] = 'iOS'
        desired_caps['browserName']=''
        desired_caps['automationName'] = 'XCUITest'
        desired_caps['platformVersion'] = '10.3'
        desired_caps['deviceName'] = 'iPad'
        desired_caps['app'] = os.path.abspath('../pnlPianoClassing_teacher_iPad.ipa')
        desired_caps['udid'] = '4328fd8e3a20ebc4d79dd6e65059b563555dbfe2'
        desired_caps['fullReset'] = True
        #desired_caps['clearSystemFiles'] = True
        desired_caps['xcodeOrgId'] = 'P2ZL3LJVPZ'
        #desired_caps['xcodeSigning'] = 'iPhone Developer'

        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
        #self.driver.implicitly_wait(30)
        sleep(3)

    def edit_send_Classshet(self):
        driver=self.driver
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n002:未发送课单:编辑后保存再发送课单----开始:'+now)
        login(self)
        sleep(3)
        #本日暂时没有课程安排
        """
        lis1=driver.find_elements_by_accessibility_id('本日暂时没有课程安排')
        if len(lis1)!=0:
            for i in range(2,8):
                #bu=driver.find_elements_by_class_name('XCUIElementTypeButton')
                bu=driver.find_element_by_xpath('//XCUIElementTypeCell/XCUIElementTypeButton['+str(i)+']')
                bu.click()
                sleep(2)
                lis2=driver.find_elements_by_accessibility_id('课程要求')
                if len(lis2)!=0:
                    #课程要求 middle
                    driver.find_element_by_xpath('//XCUIElementTypeOther/XCUIElementTypeButton').click()
                    sleep(2)
                    break
        else:
            #课程要求 top
            driver.find_element_by_xpath('//XCUIElementTypeOther/XCUIElementTypeButton').click()
            sleep(2)
        """
        driver.find_element_by_xpath('//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton').click()
        sleep(3)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf0='./'+now+'_002b_beforeSend_R.png'
        driver.get_screenshot_as_file(sf0)
        sleep(3)
        driver.find_element_by_accessibility_id('编辑陪练单').click()
        sleep(3)
        t1=EC.alert_is_present()(driver)
        if t1:
            t1.accept()
            sleep(3)
        #上课表现
        driver.find_elements_by_class_name('XCUIElementTypeButton')[1].click()
        sleep(2)
        driver.find_element_by_accessibility_id('很好').click()
        sleep(1)
        #音符准确度
        driver.find_elements_by_class_name('XCUIElementTypeButton')[2].click()
        sleep(2)
        driver.find_element_by_accessibility_id('较好').click()
        sleep(1)
        #节奏准确度
        driver.find_elements_by_class_name('XCUIElementTypeButton')[3].click()
        sleep(2)
        driver.find_element_by_accessibility_id('一般').click()
        sleep(1)
        #连贯性
        driver.find_elements_by_class_name('XCUIElementTypeButton')[4].click()
        sleep(2)
        driver.find_element_by_accessibility_id('尚可').click()
        sleep(1)
        #请填写本节课的陪练曲目，下节课的备注。
        edit=driver.find_element_by_class_name('XCUIElementTypeTextView')
        edit.click()
        edit.set_value('1234567890陪练曲')
        sleep(1)
        delete=driver.find_elements_by_accessibility_id('deleteRecord')
        if len(delete)!=0:
            driver.find_element_by_accessibility_id('deleteRecord').click()
            sleep(2)
            driver.find_element_by_accessibility_id('确定').click()
            sleep(2)
        driver.find_element_by_accessibility_id('micophone').click()
        sleep(10)
        driver.find_element_by_accessibility_id('点击停止录音').click()
        sleep(2)
        driver.find_element_by_accessibility_id('ic play2').click()
        sleep(6)
        driver.find_element_by_accessibility_id('ic time out').click()
        sleep(2)
        driver.swipe(660,600,0,-550,500)
        sleep(2)
        driver.swipe(660,600,0,-550,500)
        sleep(2)
        driver.find_element_by_accessibility_id('存为草稿').click()
        sleep(2)
        driver.find_element_by_accessibility_id('确定').click()
        sleep(5)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf1='./'+now+'_002b_afterSave_R.png'
        driver.get_screenshot_as_file(sf1)
        sleep(2)
        driver.find_element_by_accessibility_id('编辑陪练单').click()
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf2='./'+now+'_002b_ClassSheet1_R.png'
        driver.get_screenshot_as_file(sf2)
        sleep(2)
        driver.swipe(660,600,0,-550,500)
        sleep(2)
        driver.swipe(660,600,0,-550,500)
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf3='./'+now+'_002b_ClassSheet2_R.png'
        driver.get_screenshot_as_file(sf3)
        sleep(2)
        driver.find_element_by_accessibility_id('提交陪练单').click()
        sleep(2)
        driver.find_element_by_accessibility_id('确定').click()
        sleep(5)
        driver.find_element_by_accessibility_id('red back').click()
        sleep(2)
        driver.find_element_by_accessibility_id('陪练单').click()
        sleep(2)
        t2=EC.alert_is_present()(driver)
        if t2:
            t2.accept()
            sleep(3)
        driver.find_element_by_accessibility_id('已发送陪练单').click()
        sleep(2)
        #driver.swipe(660,600,0,-400,1000)
        #sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf4='./'+now+'_002b_sentClassSheet_R.png'
        driver.get_screenshot_as_file(sf4)
        sleep(3)
        logout(self)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n002:未发送课单:编辑后保存再发送课单----结束:'+now)
    
    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    testunit=unittest.TestSuite()
    testunit.addTest(TestTeacher('edit_send_Classshet'))
    now=time.strftime('%Y-%m-%d %H_%M_%S')
    filename='./'+now+'_002b_R.html'
    fp=open(filename,'wb')
    runner=HTMLTestRunner(stream=fp,title='测试老师版iOS10.3.3/iPad真机(未发送课单:编辑后保存再发送课单)测试报告by Appium',
                          description='自动化测试脚本运行状态:')
    runner.run(testunit)
    fp.close()
