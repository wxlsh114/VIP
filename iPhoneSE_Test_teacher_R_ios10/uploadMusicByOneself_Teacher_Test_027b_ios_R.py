#coding=utf-8
from appium import webdriver
import unittest,time,os
from time import sleep
from HTMLTestRunner import HTMLTestRunner
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.support import expected_conditions as EC
from pub_Teacher import login,logout,turnpage_play

class TestTeacher(unittest.TestCase):

    def setUp(self):
        # set up appium
        desired_caps = {}
        #desired_caps['appium-version'] = '1.7.1'
        desired_caps['platformName'] = 'iOS'
        desired_caps['browserName']=''
        desired_caps['automationName'] = 'XCUITest'
        desired_caps['platformVersion'] = '10.3'
        desired_caps['deviceName'] = 'iPhoneSE'
        desired_caps['app'] = os.path.abspath('../Text-Vip-Gangqin-Teacher.ipa')
        desired_caps['udid'] = 'efa32991905b2cb5d9fdf2bbf66ba5920cbf79e7'
        desired_caps['fullReset'] = True
        desired_caps['clearSystemFiles'] = True
        desired_caps['xcodeOrgId'] = 'P2ZL3LJVPZ'
        #desired_caps['xcodeSigning'] = 'iPhone Developer'

        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
        #self.driver.implicitly_wait(30)
        sleep(3)

    def uploadMusicByOneself(self):
        driver=self.driver
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n027:自主上传乐谱----开始:'+now)
        login(self)
        sleep(4)
        #本日暂时没有课程安排;can't see in iPhoneSE
        lis1=driver.find_elements_by_accessibility_id('pic_1')
        if len(lis1)!=0:
            for i in range(2,8):
                #bu=driver.find_elements_by_class_name('XCUIElementTypeButton')
                bu=driver.find_element_by_xpath('//XCUIElementTypeOther[1]/XCUIElementTypeButton['+str(i)+']')
                bu.click()
                sleep(3)
                lis2=driver.find_elements_by_accessibility_id('查看乐谱')
                if len(lis2)!=0:
                    #查看乐谱 middle
                    driver.find_element_by_accessibility_id('查看乐谱').click()
                    sleep(2)
                    break
        else:
            #查看乐谱 top
            driver.find_element_by_accessibility_id('查看乐谱').click()
            sleep(2)
        sleep(2)
        at=EC.alert_is_present()(driver)
        if at:
            at.accept()
            sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf='./'+now+'_027b_uploadedMusicItems_R.png'
        driver.get_screenshot_as_file(sf)
        sleep(2)
        flag=driver.find_elements_by_accessibility_id('课程已结束')
        if len(flag)!=0:
            print('\n课程已结束: True')
        else:
            print('\n课程已结束: False')
        sleep(1)
        items=driver.find_elements_by_class_name('XCUIElementTypeCell')
        i=len(items)
        print(str(i))
        sleep(2)
        if (i!=0 and len(flag)==0):
            for j in range(i):
                driver.find_element_by_xpath('//XCUIElementTypeButton[@name="删除"]').click()
                sleep(2)
                #//XCUIElementTypeButton[@name="确定"]
                driver.find_element_by_accessibility_id('确定').click()
                sleep(2)
        driver.find_element_by_accessibility_id('上传乐谱').click()
        sleep(2)
        driver.find_element_by_accessibility_id('自主上传').click()
        sleep(2)
        driver.find_element_by_accessibility_id('取消').click()
        sleep(2)
        driver.find_element_by_accessibility_id('自主上传').click()
        sleep(2)
        driver.find_element_by_accessibility_id('最近上过的乐谱').click()
        sleep(2)
        no=driver.find_elements_by_accessibility_id('该学生暂无上过的乐谱')
        if len(no)==0:
            driver.find_element_by_accessibility_id('添加').click()
            sleep(3)
            driver.find_element_by_accessibility_id('ic nav back').click()
            sleep(2)
            driver.find_element_by_accessibility_id('ic nav back').click()
            sleep(2)
            now=time.strftime('%Y-%m-%d %H_%M_%S')
            sf0='./'+now+'_027b_uploadedMusicBybefore_R.png'
            driver.get_screenshot_as_file(sf0)
            sleep(2)
            driver.find_elements_by_class_name('XCUIElementTypeCell')[0].click()
            sleep(8)
            now=time.strftime('%Y-%m-%d %H_%M_%S')
            sf2='./'+now+'_027b_uploadedMusicByAlbeforeDetail_R.png'
            driver.get_screenshot_as_file(sf2)
            sleep(2)
            turnpage_play(self)
            sleep(2)
            driver.find_element_by_accessibility_id('ic nav back').click()
            sleep(2)
            t=0
        else:
            driver.find_element_by_accessibility_id('ic nav back').click()
            sleep(2)
            driver.find_element_by_accessibility_id('ic nav back').click()
            sleep(2)
            t=1
        driver.find_element_by_accessibility_id('上传乐谱').click()
        sleep(2)
        driver.find_element_by_accessibility_id('自主上传').click()
        sleep(3)
        driver.find_element_by_accessibility_id('从相册选择').click()
        sleep(3)
        driver.find_elements_by_class_name('XCUIElementTypeButton')[5].click()
        sleep(2)
        driver.find_element_by_accessibility_id('完成').click()
        sleep(10)
        driver.find_element_by_accessibility_id('ic nav back').click()
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf2='./'+now+'_027b_uploadedMusicByAlbum_R.png'
        driver.get_screenshot_as_file(sf2)
        sleep(2)
        driver.find_elements_by_class_name('XCUIElementTypeCell')[1-t].click()
        sleep(6)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf2='./'+now+'_027b_uploadedMusicByAlbumDetail_R.png'
        driver.get_screenshot_as_file(sf2)
        sleep(2)
        driver.find_element_by_accessibility_id('ic nav back').click()
        sleep(2)
        driver.find_element_by_accessibility_id('上传乐谱').click()
        sleep(2)
        driver.find_element_by_accessibility_id('自主上传').click()
        sleep(2)
        driver.find_element_by_accessibility_id('拍照上传').click()
        sleep(2)
        driver.find_element_by_accessibility_id('ic_camera').click()
        sleep(3)
        #hao
        at2=EC.alert_is_present()(driver)
        if at2:
            at2.accept()
            sleep(2)
        driver.find_element_by_xpath('//XCUIElementTypeButton[@name="FrontBackFacingCameraChooser"]').click()
        #TouchAction(self.driver).press(x=288,y=530).wait(100).release().perform()
        sleep(4)
        #PhotoCapture
        driver.find_element_by_accessibility_id('PhotoCapture').click()
        sleep(2)
        driver.find_element_by_accessibility_id('使用照片').click()
        sleep(3)
        driver.find_element_by_accessibility_id('完成').click()
        sleep(13)
        driver.find_element_by_accessibility_id('ic nav back').click()
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf1='./'+now+'_027b_uploadedMusicBySelfie_R.png'
        driver.get_screenshot_as_file(sf1)
        sleep(2)
        driver.find_elements_by_class_name('XCUIElementTypeCell')[2-t].click()
        sleep(8)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf2='./'+now+'_027b_uploadedMusicBySelfieDetail_R.png'
        driver.get_screenshot_as_file(sf2)
        sleep(3)
        driver.find_element_by_accessibility_id('ic nav back').click()
        sleep(2)
        driver.find_element_by_accessibility_id('ic nav back').click()
        sleep(3)
        logout(self)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n027:自主上传乐谱----结束:'+now)

    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    testunit=unittest.TestSuite()
    testunit.addTest(TestTeacher('uploadMusicByOneself'))
    now=time.strftime('%Y-%m-%d %H_%M_%S')
    filename='./'+now+'_027b_R.html'
    fp=open(filename,'wb')
    runner=HTMLTestRunner(stream=fp,title='测试老师版iOS10.3.3/iPhoneSE真机(自主上传乐谱)测试报告by Appium',
                          description='自动化测试脚本运行状态:')
    runner.run(testunit)
    fp.close()
