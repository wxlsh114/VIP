#coding=UTF-8
from macaca import WebDriver
import unittest,time,os
from time import sleep
from HTMLTestRunner import HTMLTestRunner
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
#from selenium.webdriver.common.action_chains import ActionChains

desired_caps = {
        'platformName': 'iOS',
        'platformVersion': '10.3',
        'deviceName': 'iPhone6',
        'app': os.path.abspath('../VIPStudent.ipa'),
        'bundleId':'com.Pnlyy.developmentVIPStudent',
        'udid':'851c908fafaa15e592e2145131cc202cd20cc977',
        #'autoDismissAlerts': True,
        'autoAcceptAlerts': True
}

server_url = {
    'hostname': 'localhost',
    'port': 3456
}

class TestStudent(unittest.TestCase):

    def setUp(self):
        # set up macaca
        self.driver = WebDriver(desired_caps, server_url)
        self.driver.init()
        sleep(2)

    def Login(self):
        driver=self.driver
        #driver.element_by_name('允许').click()
        sleep(2)
        """
        driver.touch('drag',{'fromX':350,'fromY':500,'toX':30,'toY':500})
        sleep(1)
        driver.touch('drag',{'fromX':350,'fromY':500,'toX':30,'toY':500})
        sleep(1)
        driver.element_by_name('马 上 体 验').click()
        sleep(1)
        """
        #登录
        #driver.element_by_xpath('//XCUIElementTypeButton[@name="登录"]').click()
        driver.element_by_name('跳 过').click()
        sleep(2)
        mo=driver.element_by_class_name('XCUIElementTypeTextField')
        mo.click()
        mo.send_keys('18089897878')
        #driver.element_by_name('完成').click()
        sleep(1)
        pwd=driver.element_by_class_name('XCUIElementTypeSecureTextField')
        pwd.click()
        pwd.send_keys('123456')
        #driver.element_by_name('完成').click()
        #name=登录
        driver.elements_by_class_name('XCUIElementTypeButton')[2].click()
        #driver.element_by_name('登录').click()
        sleep(3)
        #someone already logged in
        if driver.element_by_name_if_exists('确定'):
            driver.element_by_name('确定').click()
            sleep(3)
        if driver.element_by_name_if_exists('好'):
            driver.element_by_name('好').click()
            sleep(3)
        #test now
        driver.element_by_name('开始测试').click()
        #driver.element_by_xpath('//*[@name="开始测试"]').click()
        sleep(3)
        driver.element_by_name('点击开始录音').click()
        sleep(3)
        driver.element_by_name('停止录音').click()
        sleep(2)
        driver.element_by_name('有听到声音').click()
        sleep(2)
        driver.element_by_name('下一步').click()
        sleep(2)
        driver.element_by_name('下一步').click()
        sleep(2)
        driver.element_by_name('完成测试').click()
        sleep(3)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf3='./'+now+'_001_login_macaca_R.png'
        driver.save_screenshot(sf3)
        sleep(2)
        #logout
        driver.element_by_name('个人中心').click()
        sleep(2)
        driver.touch('drag',{'fromX':600,'fromY':500,'toX':600,'toY':350})
        sleep(2)
        driver.element_by_name('退出登录').click()
        sleep(2)
        driver.element_by_name('确定').click()
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf2='./'+now+'_001_logout_macaca_R.png'
        driver.save_screenshot(sf2)
        sleep(2)
        
    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    testunit=unittest.TestSuite()
    testunit.addTest(TestStudent('Login'))
    now=time.strftime('%Y-%m-%d %H_%M_%S')
    filename='./'+now+'_001_macaca_R.html'
    fp=open(filename,'wb')
    runner=HTMLTestRunner(stream=fp,title='测试学生版iOS10.3.3/iPhone6真机(用户登录)测试报告by Macaca',
                          description='自动化测试脚本运行状态:')
    runner.run(testunit)
    fp.close()
