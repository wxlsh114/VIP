#coding=utf-8
from appium import webdriver
import unittest,time,os
from time import sleep
from HTMLTestRunner import HTMLTestRunner
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from appium.webdriver.common.touch_action import TouchAction
from pub_Teacher import login,logout

class TestTeacher(unittest.TestCase):

    def setUp(self):
        # set up appium
        desired_caps = {}
        #desired_caps['appium-version'] = '1.6.5'
        desired_caps['platformName'] = 'iOS'
        desired_caps['browserName']=''
        desired_caps['automationName'] = 'XCUITest'
        desired_caps['platformVersion'] = '10.3'
        desired_caps['deviceName'] = 'iPhoneSE'
        desired_caps['app'] = os.path.abspath('../Text-Vip-Gangqin-Teacher.ipa')
        desired_caps['udid'] = 'efa32991905b2cb5d9fdf2bbf66ba5920cbf79e7'
        desired_caps['fullReset'] = True
        desired_caps['clearSystemFiles'] = True
        desired_caps['xcodeOrgId'] = 'P2ZL3LJVPZ'
        #desired_caps['xcodeSigning'] = 'iPhone Developer'

        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
        #self.driver.implicitly_wait(30)
        sleep(2)

    def checkNet(self):
        driver=self.driver
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n026:联系客服----开始:'+now)
        login(self)
        sleep(3)
        driver.find_element_by_xpath('//XCUIElementTypeButton[@name="网络检测"]').click()
        sleep(10)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf0='./'+now+'_036b_checkNetwork_R.png'
        driver.save_screenshot(sf0)
        sleep(2)
        driver.find_element_by_xpath('//XCUIElementTypeButton[@name="我知道了"]').click()
        sleep(3)
        logout(self)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n026:联系客服----结束:'+now)
    
    
    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    testunit=unittest.TestSuite()
    testunit.addTest(TestTeacher('checkNet'))
    now=time.strftime('%Y-%m-%d %H_%M_%S')
    filename='./'+now+'_036b_R.html'
    fp=open(filename,'wb')
    runner=HTMLTestRunner(stream=fp,title='测试老师版iOS10.3.3/iPhoneSE真机(网络检测)测试报告by Appium',
                          description='自动化测试脚本运行状态:')
    runner.run(testunit)
    fp.close()
