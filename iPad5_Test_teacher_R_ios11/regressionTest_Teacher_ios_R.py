#coding=utf-8
import unittest,time,os
from time import sleep
from appium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from HTMLTestRunner import HTMLTestRunner
from appium.webdriver.common.touch_action import TouchAction
from pub_Teacher import login,logout,turnpage_play

import logIn_Teacher_Test_001b_ios_R
import unsentClassSheet_Teacher_Test_002b_ios_R
import searchMusic_Teacher_Test_003b_ios_R
import checkSearchMusic_Teacher_Test_004b_ios_R
import uploadMusic_Teacher_Test_005b_ios_R
import checkHistoryClass_Teacher_Test_006b_ios_R
import mySalary_Teacher_Test_007b_ios_R
import testDevice_Teacher_Test_008b_ios_R
import waitForStuMorethan1Min_Teacher_Test_009b_ios_R
import aboutUs_Teacher_Test_010b_ios_R
import generalSetting_Teacher_Test_011b_ios_R
import logOut_Teacher_Test_012b_ios_R
import uploadMusicFromClassroom_Teacher_Test_013b_ios_R
import checkClassNote_Teacher_Test_014b_ios_R
import bottomClassSheet_Teacher_Test_015b_ios_R
import personalCenter_Teacher_Test_016b_ios_R
import bottomMusic_Teacher_Test_017b_ios_R
import advice_Teacher_Test_018b_ios_R
import exitClassroom_Teacher_Test_019b_ios_R
import switchLine_Teacher_Test_020b_ios_R
import resetClassroom_Teacher_Test_021b_ios_R
import waitForStudent_Teacher_Test_022b_ios_R
import classUI_Teacher_Test_023b_ios_R
import sentClassSheet_Teacher_Test_024b_ios_R
import changePassword_Teacher_Test_025b_ios_R
import callCustomerService_Teacher_Test_026b_ios_R
import uploadMusicByOneself_Teacher_Test_027b_ios_R
import displayAndPlayMusic_Teacher_Test_028b_ios_R
import checkClassMusic_Teacher_Test_029b_ios_R
import waitStuCheckClassNote_Teacher_Test_030b_ios_R
import waitStuCheckMusic_Teacher_Test_031b_ios_R
import uploadMusicByMeClassroom_Teacher_Test_032b_ios_R
import searchMusicFromClassroom_Teacher_Test_033b_ios_R
import deleteEditMusicAfterClassEnd_Teacher_Test_034b_ios_R
import deleteEditMusicNotAfterClassEnd_Teacher_Test_035b_ios_R
import checkNetwork_Teacher_Test_036b_ios_R

# Returns abs path relative to this file and not cwd
PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)


class TestTeacher(unittest.TestCase):

    def setUp(self):
        desired_caps = {}
        desired_caps['platformName'] = 'iOS'
        desired_caps['browserName']=''
        desired_caps['automationName'] = 'XCUITest'
        desired_caps['platformVersion'] = '11.0'
        desired_caps['deviceName'] = 'iPad'
        desired_caps['app'] = os.path.abspath('../pnlPianoClassing_teacher_iPad.ipa')
        desired_caps['udid'] = '06a3e0b8854c2626c83be6ce191ecd775510acfa'
        desired_caps['fullReset'] = True
        #desired_caps['clearSystemFiles'] = True
        desired_caps['xcodeOrgId'] = 'P2ZL3LJVPZ'
        #desired_caps['xcodeSigning'] = 'iPhone Developer'

        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
        sleep(2)

    def tearDown(self):
        # end the session
        self.driver.quit()

if __name__ == '__main__':
    testunit=unittest.TestSuite()
    testunit.addTest(logIn_Teacher_Test_001b_ios_R.TestTeacher('Login'))
    #testunit.addTest(unsentClassSheet_Teacher_Test_002b_ios_R.TestTeacher('edit_send_Classshet'))
    testunit.addTest(searchMusic_Teacher_Test_003b_ios_R.TestTeacher('searchMusic'))
    testunit.addTest(checkSearchMusic_Teacher_Test_004b_ios_R.TestTeacher('checkSearchMusic'))
    testunit.addTest(uploadMusic_Teacher_Test_005b_ios_R.TestTeacher('uploadMusic'))
    testunit.addTest(checkHistoryClass_Teacher_Test_006b_ios_R.TestTeacher('checkHistoryClass'))
    testunit.addTest(mySalary_Teacher_Test_007b_ios_R.TestTeacher('mySalary'))
    testunit.addTest(testDevice_Teacher_Test_008b_ios_R.TestTeacher('testDevice'))
    testunit.addTest(waitForStuMorethan1Min_Teacher_Test_009b_ios_R.TestTeacher('waitForStuMorethan1Min'))
    testunit.addTest(aboutUs_Teacher_Test_010b_ios_R.TestTeacher('aboutUs'))
    testunit.addTest(generalSetting_Teacher_Test_011b_ios_R.TestTeacher('generalSetting'))
    testunit.addTest(logOut_Teacher_Test_012b_ios_R.TestTeacher('Logout'))
    testunit.addTest(uploadMusicFromClassroom_Teacher_Test_013b_ios_R.TestTeacher('uploadMusicFromClassroom'))
    testunit.addTest(checkClassNote_Teacher_Test_014b_ios_R.TestTeacher('checkClassNote'))
    testunit.addTest(bottomClassSheet_Teacher_Test_015b_ios_R.TestTeacher('bottomClassshet'))
    testunit.addTest(personalCenter_Teacher_Test_016b_ios_R.TestTeacher('personalCenter'))
    testunit.addTest(bottomMusic_Teacher_Test_017b_ios_R.TestTeacher('bottomMusic'))
    testunit.addTest(advice_Teacher_Test_018b_ios_R.TestTeacher('advice'))
    testunit.addTest(exitClassroom_Teacher_Test_019b_ios_R.TestTeacher('exitClassroom'))
    #no switch line button
    #testunit.addTest(switchLine_Teacher_Test_020b_ios_R.TestTeacher('switchLine'))
    testunit.addTest(resetClassroom_Teacher_Test_021b_ios_R.TestTeacher('resetClassroom'))
    testunit.addTest(waitForStudent_Teacher_Test_022b_ios_R.TestTeacher('waitForStudent'))
    testunit.addTest(classUI_Teacher_Test_023b_ios_R.TestTeacher('classUI'))
    testunit.addTest(sentClassSheet_Teacher_Test_024b_ios_R.TestTeacher('sentClassshet'))
    #025b moved to the end of this TestSuite 
    testunit.addTest(callCustomerService_Teacher_Test_026b_ios_R.TestTeacher('callService'))
    testunit.addTest(uploadMusicByOneself_Teacher_Test_027b_ios_R.TestTeacher('uploadMusicByOneself'))
    testunit.addTest(displayAndPlayMusic_Teacher_Test_028b_ios_R.TestTeacher('displayPlayMusic'))
    testunit.addTest(checkClassMusic_Teacher_Test_029b_ios_R.TestTeacher('checkClassMusic'))
    testunit.addTest(waitStuCheckClassNote_Teacher_Test_030b_ios_R.TestTeacher('waitStuCheckClassNote'))
    testunit.addTest(waitStuCheckMusic_Teacher_Test_031b_ios_R.TestTeacher('waitStuCheckMusic'))
    testunit.addTest(uploadMusicByMeClassroom_Teacher_Test_032b_ios_R.TestTeacher('uploadMusicByOneselfClassr'))
    testunit.addTest(searchMusicFromClassroom_Teacher_Test_033b_ios_R.TestTeacher('searchAddMusicClassroom'))
    testunit.addTest(deleteEditMusicAfterClassEnd_Teacher_Test_034b_ios_R.TestTeacher('deleteEditMusicEnd'))
    testunit.addTest(deleteEditMusicNotAfterClassEnd_Teacher_Test_035b_ios_R.TestTeacher('deleteEditMusicNotEnd'))
    testunit.addTest(checkNetwork_Teacher_Test_036b_ios_R.TestTeacher('checkNet'))
    testunit.addTest(changePassword_Teacher_Test_025b_ios_R.TestTeacher('changePwd'))
    testunit.addTest(changePassword_Teacher_Test_025b_ios_R.TestTeacher('changePwdBack'))
    now=time.strftime('%Y-%m-%d %H_%M_%S')
    filename='./'+now+'_regressionTest_001_R.html'
    fp=open(filename,'wb')
    runner=HTMLTestRunner(stream=fp,title='测试老师版iOS11.0.3/iPad5真机[回归测试]测试报告by Appium',
                          description='自动化测试脚本集运行状态:')
    runner.run(testunit)
    fp.close()
