#coding=utf-8
from appium import webdriver
import unittest,time,os
from time import sleep
from HTMLTestRunner import HTMLTestRunner
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from appium.webdriver.common.touch_action import TouchAction
from pub_Teacher import login,logout,turnpage_play

class TestTeacher(unittest.TestCase):

    def setUp(self):
        # set up appium
        desired_caps = {}
        #desired_caps['appium-version'] = '1.7.1'
        desired_caps['platformName'] = 'iOS'
        desired_caps['browserName']=''
        desired_caps['automationName'] = 'XCUITest'
        desired_caps['platformVersion'] = '11.0'
        desired_caps['deviceName'] = 'iPhone6'
        desired_caps['app'] = os.path.abspath('../Text-Vip-Gangqin-Teacher.ipa')
        desired_caps['udid'] = '851c908fafaa15e592e2145131cc202cd20cc977'
        desired_caps['fullReset'] = True
        desired_caps['clearSystemFiles'] = True
        desired_caps['xcodeOrgId'] = 'P2ZL3LJVPZ'
        desired_caps['xcodeSigning'] = 'iPhone Developer'

        self.driver = webdriver.Remote('http://127.0.0.1:4723/wd/hub', desired_caps)
        #self.driver.implicitly_wait(30)
        sleep(2)

    def uploadMusicByOneself(self):
        driver=self.driver
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n027:自主上传乐谱----开始:'+now)
        login(self)
        sleep(3)
        lis1=driver.find_elements_by_accessibility_id('本日暂时没有课程安排')
        if len(lis1)!=0:
            for i in range(7,13):
                bu=driver.find_elements_by_class_name('XCUIElementTypeButton')
                bu[i].click()
                sleep(2)
                lis2=driver.find_elements_by_accessibility_id('查看乐谱')
                if len(lis2)!=0:
                    #查看乐谱 middle
                    driver.find_element_by_accessibility_id('查看乐谱').click()
                    sleep(2)
                    break
        else:
            #查看乐谱 top
            driver.find_element_by_accessibility_id('ic_sheet_music').click()
            sleep(2)
        o=driver.find_elements_by_accessibility_id('好')
        if len(o)!=0:
            driver.find_element_by_accessibility_id('好').click()
            sleep(3)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf='./'+now+'_027b_uploadedMusicItems_R.png'
        driver.get_screenshot_as_file(sf)
        sleep(2)
        items=driver.find_elements_by_class_name('XCUIElementTypeCell')
        i=len(items)
        print(str(i))
        sleep(2)
        if i!=0:
            for j in range(i):
                driver.find_element_by_accessibility_id('删除').click()
                sleep(3)
                driver.find_element_by_accessibility_id('确定').click()
                sleep(2)
        driver.find_element_by_accessibility_id('上传乐谱').click()
        sleep(3)
        driver.find_element_by_accessibility_id('自主上传').click()
        sleep(3)
        driver.find_element_by_accessibility_id('取消').click()
        sleep(2)
        driver.find_element_by_accessibility_id('自主上传').click()
        sleep(2)
        driver.find_element_by_accessibility_id('最近上过的乐谱').click()
        sleep(2)
        driver.find_element_by_accessibility_id('添加').click()
        sleep(3)
        driver.find_element_by_accessibility_id('ic nav back').click()
        sleep(2)
        driver.find_element_by_accessibility_id('ic nav back').click()
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf0='./'+now+'_027b_uploadedMusicBybefore_R.png'
        driver.get_screenshot_as_file(sf0)
        sleep(2)
        driver.find_elements_by_class_name('XCUIElementTypeCell')[0].click()
        sleep(8)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf2='./'+now+'_027b_uploadedMusicByAlbeforeDetail_R.png'
        driver.get_screenshot_as_file(sf2)
        sleep(2)
        turnpage_play(self)
        sleep(2)
        driver.find_element_by_accessibility_id('ic nav back').click()
        sleep(2)
        driver.find_element_by_accessibility_id('上传乐谱').click()
        sleep(3)
        driver.find_element_by_accessibility_id('自主上传').click()
        sleep(3)
        driver.find_element_by_accessibility_id('从相册选择').click()
        sleep(3)
        driver.find_elements_by_class_name('XCUIElementTypeButton')[5].click()
        sleep(2)
        driver.find_element_by_accessibility_id('完成').click()
        sleep(10)
        driver.find_element_by_accessibility_id('ic nav back').click()
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf2='./'+now+'_027b_uploadedMusicByAlbum_R.png'
        driver.get_screenshot_as_file(sf2)
        sleep(2)
        driver.find_elements_by_class_name('XCUIElementTypeCell')[1].click()
        sleep(6)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf2='./'+now+'_027b_uploadedMusicByAlbumDetail_R.png'
        driver.get_screenshot_as_file(sf2)
        sleep(2)
        driver.find_element_by_accessibility_id('ic nav back').click()
        sleep(2)
        driver.find_element_by_accessibility_id('上传乐谱').click()
        sleep(2)
        driver.find_element_by_accessibility_id('自主上传').click()
        sleep(2)
        driver.find_element_by_accessibility_id('拍照上传').click()
        sleep(2)
        driver.find_element_by_accessibility_id('ic_camera').click()
        sleep(3)
        driver.find_element_by_accessibility_id('好').click()
        sleep(3)
        driver.find_element_by_xpath('//XCUIElementTypeButton[@name="FrontBackFacingCameraChooser"]').click()
        #TouchAction(self.driver).press(x=343,y=619).wait(100).release().perform()
        sleep(3)
        #PhotoCapture
        driver.find_element_by_accessibility_id('PhotoCapture').click()
        sleep(3)
        driver.find_element_by_accessibility_id('使用照片').click()
        sleep(3)
        driver.find_element_by_accessibility_id('完成').click()
        sleep(13)
        driver.find_element_by_accessibility_id('ic nav back').click()
        sleep(2)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf1='./'+now+'_027b_uploadedMusicBySelfie_R.png'
        driver.get_screenshot_as_file(sf1)
        sleep(2)
        driver.find_elements_by_class_name('XCUIElementTypeCell')[2].click()
        sleep(8)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        sf2='./'+now+'_027b_uploadedMusicBySelfieDetail_R.png'
        driver.get_screenshot_as_file(sf2)
        sleep(2)
        driver.find_element_by_accessibility_id('ic nav back').click()
        sleep(2)
        driver.find_element_by_accessibility_id('ic nav back').click()
        sleep(3)
        logout(self)
        now=time.strftime('%Y-%m-%d %H_%M_%S')
        print('\n027:自主上传乐谱----结束:'+now)

    def tearDown(self):
        self.driver.quit()

if __name__ == '__main__':
    testunit=unittest.TestSuite()
    testunit.addTest(TestTeacher('uploadMusicByOneself'))
    now=time.strftime('%Y-%m-%d %H_%M_%S')
    filename='./'+now+'_027b_R.html'
    fp=open(filename,'wb')
    runner=HTMLTestRunner(stream=fp,title='测试老师版iOS11.0.2/iPhone6真机(自主上传乐谱)测试报告by Appium',
                          description='自动化测试脚本运行状态:')
    runner.run(testunit)
    fp.close()
